#!/bin/bash
# function in bash

# This code will start ec2 instances if they are not running.
# This code will stop ec2 instances if they are running.

INSTANCE_IDS=("i-02bdc8281ecc83506" "i-0f8aa18423b137a44")

# Start Ec2 Instance
function start_ec2() {
    REGION=$1

    for id in "${INSTANCE_IDS[@]}"; do
        echo "Starting instance: $id"
        status=$(describe_ec2 $REGION $id)
        if [[ $status == "running" ]]; then
            echo "Instance $id is running"
        else
            echo "Instance $id is not running"
            aws ec2 start-instances --instance-ids "$id" --region "$REGION"
        fi
    done
}

# Stop Ec2 Instance
function stop_ec2() {
    REGION=$1
    for id in "${INSTANCE_IDS[@]}"; do
        echo "Stopping instance: $id"
        status=$(describe_ec2 $REGION $id)
        if [[ $status == "stopped" || $status == "terminated" ]]; then
            echo "Instance $id is stopped"
        else
            echo "Instance $id is not stopped"
            aws ec2 stop-instances --instance-ids "$id" --region "$REGION"
        fi
    done
}

# Get Ec2 Instance Status
function describe_ec2() {
    REGION=$1
    INSTANCEID=$2
    state=$(aws ec2 describe-instances --instance-ids "$INSTANCEID" --region "$REGION" | python -c "import sys, json; print json.load(sys.stdin)['Reservations'][0]['Instances'][0]['State']['Name']")
    echo $state
}

if [ $# -ne 2 ]; then
    echo "Usage: $0 start <region>|stop <region>"
    exit 1
fi

if [ "$1" == "start" ]; then
    start_ec2 $2
elif [ "$1" == "stop" ]; then
    stop_ec2 $2
else
    echo "Usage: $0 start <region>|stop <region>"
    exit 1
fi