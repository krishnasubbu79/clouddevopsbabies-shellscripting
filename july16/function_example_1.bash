#!/bin/bash
# Global variables, local variables
# pass variables to functions
# function in bash
function rectangle_area() {
    area=$(($1 * $2))
    echo $area
}

area=$(rectangle_area 20 30)
echo "Area of rectangle is: $area"
