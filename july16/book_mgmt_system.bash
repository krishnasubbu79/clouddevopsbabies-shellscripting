#!/bin/bash
function add_books() {
    local book_name
    echo "Enter the name of the book"
    read -r book_name
    BOOKS+=("$book_name")
}

function list_books() {
    for i in "${!BOOKS[@]}"; do
        echo "$i: ${BOOKS[$i]}"
    done
}

function delete_books() {
    local book_name
    echo "Enter the name of the book to delete"
    read -r book_name
    
    for index in "${!BOOKS[@]}"; do
        if [[ "${BOOKS[$index]}" == "$book_name" ]]; then
            unset BOOKS[$index]
            break
        fi
    done
}

declare -a BOOKS

while true ; do
    echo "Enter the option - add, delete, list, exit"
    read -r choice

    case $choice in
        add)
            add_books
            ;;
        list)
            list_books
            ;;
        delete) 
            delete_books
            ;;
        exit)
            exit
            ;;
    esac
done
