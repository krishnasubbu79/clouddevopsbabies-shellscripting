#!/bin/bash
# Break and Continue
LIMIT=10

echo
echo "printing numbers 1 through $LIMIT - illustrate break"

a=0
while [ $a -lt $LIMIT ]; do
    echo $a
    a=$((a+1))

    if [[ $a -eq 5 ]]; then
        echo "breaking out of loop"
        break
    fi # end if
done


echo
echo "printing numbers 1 through $LIMIT - illustrate continue"

b=0
while [ $b -lt $LIMIT ]; do
    b=$((b+1))
    if [[ $b -eq 5 ]]; then
        echo "skipping number 5"
        continue
    fi # end if
    echo $b
done