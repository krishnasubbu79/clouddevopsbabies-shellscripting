#!/bin/bash
echo "Enter the file to read : "
read -r file_name

if [[ -f $file_name ]]; then
    while read -r line; do
        echo "reading line----"
        echo "${line}"
    done < "${file_name}"
else
    echo "File does not exist"
fi

