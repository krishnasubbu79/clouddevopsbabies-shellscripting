#!/bin/bash
# Example of case statement
echo "enter a choice - 1, 2, 3, 4, 5"
read -r choice

case $choice in
    1) echo "You chose 1";;
    2) echo "You chose 2";;
    3) echo "You chose 3";;
    4) echo "You chose 4";;
    5) echo "You chose 5";;
    *) echo "You did not choose 1, 2, 3, 4, or 5";;
esac

if [[ $choice -eq 1 ]]; then
    echo "You chose 1"
elif [[ $choice -eq 2 ]]; then
    echo "You chose 2"
elif [[ $choice -eq 3 ]]; then
    echo "You chose 3"
elif [[ $choice -eq 4 ]]; then
    echo "You chose 4"
elif [[ $choice -eq 5 ]]; then
    echo "You chose 5"
else
    echo "You did not choose 1, 2, 3, 4, or 5"
fi
